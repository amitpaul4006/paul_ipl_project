const numOfTimesEachTeamOwnTossMatch = (matches) => {
  let totalOwnTossMatch = matches.reduce((totalOwnTossMatchObject, match) => {
    const toss_winner = match.toss_winner;
    const winner = match.winner;
    if (toss_winner === winner) {
      if (totalOwnTossMatchObject[toss_winner] != undefined) {
        totalOwnTossMatchObject[toss_winner] += 1;
      } else {
        totalOwnTossMatchObject[toss_winner] = 1;
      }
    }
    return totalOwnTossMatchObject;
  }, {});
  return totalOwnTossMatch;
};

module.exports = numOfTimesEachTeamOwnTossMatch;
