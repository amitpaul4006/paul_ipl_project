const topPlayerOfTheMatch = (matches) => {
  const manOfTheMatch = matches.reduce((playerOfTheMatchObject, match) => {
    let season = match.season;
    let player_of_match = match.player_of_match;

    if (playerOfTheMatchObject[season]) {
      if (playerOfTheMatchObject[season][player_of_match]) {
        playerOfTheMatchObject[season][player_of_match] += 1;
      } else {
        playerOfTheMatchObject[season][player_of_match] = 1;
      }
    } else {
      playerOfTheMatchObject[season] = {};
    }
    return playerOfTheMatchObject;
  }, {});

  //converted to array
  let manOfTheMatchArray = Object.entries(manOfTheMatch);

  //sorting, slicing,
  const topManOfTheMatch = manOfTheMatchArray.reduce(
    (topPlayerObject, element) => {
      let topPlayer = Object.entries(manOfTheMatch[element[0]])
        .sort((a, b) => b[1] - a[1])
        .slice(0, 1);
      topPlayerObject[element[0]] = topPlayer[0][0];
      return topPlayerObject;
    },
    {}
  );

  return topManOfTheMatch;
};

module.exports = topPlayerOfTheMatch;
