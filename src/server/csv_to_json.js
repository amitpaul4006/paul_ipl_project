const csv = require("csvtojson");
const fs = require("fs");

// csv data paths

const matchesCSVPath = "./src/data/matches.csv";
const deliveriesCSVPath = "./src/data/deliveries.csv";

// json data paths

const matchesJSONPath = "./src/data/matches.json";
const deliveriesJSONPath = "./src/data/deliveries.json";

//csv to json conversion function.

csv()
  .fromFile(matchesCSVPath)
  .then((matches) => {
    fs.writeFileSync(
      matchesJSONPath,
      JSON.stringify(matches, null, 4),
      "utf-8",
      (error) => {
        if (error) console.log(error);
      }
    );

    
    csv()
      .fromFile(deliveriesCSVPath)
      .then((deliveries) => {
        fs.writeFileSync(
          deliveriesJSONPath,
          JSON.stringify(deliveries, null, 4),
          "utf8",
          (error) => {
            if (error) console.log(error);
          }
        );
      });
  });
