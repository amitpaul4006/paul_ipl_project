const fs = require("fs");

//functions
const saveData = require("./save_data");
const numberOfMatchesPlayedPerYear = require("./number_matches_played_per_year");
const numOfMatchesOwnPerTeamPerYear = require("./num_of_matches_own_team_year");
const extraRunPerTeam = require("./extra_run_per_team");
const top10EconomicalBowlers = require("./top_10_economical_bowlers");
const numOfTimesEachTeamOwnTossMatch = require("./num_times_each_team_own_toss_match");
const topPlayerOfTheMatch = require("./top_player_of_the_match");
const strikeRateBatsman = require("./strike_rate_batsman_each_season");
const highestNumTimesPlayerDismissed = require("./highest_num_times_player_dismissed");
const bestEconomyBowlerSuperOver = require("./best_economy_bowler_superOver");

//json data paths
const mathcesJSONPath = "./src/data/matches.json";
const deliveriesJSONPath = "./src/data/deliveries.json";

//json data
const matches = JSON.parse(fs.readFileSync(mathcesJSONPath));
const deliveries = JSON.parse(fs.readFileSync(deliveriesJSONPath));

//saving the data with save function

saveData(
  numberOfMatchesPlayedPerYear(matches),
  numOfMatchesOwnPerTeamPerYear(matches),
  extraRunPerTeam(matches, deliveries),
  top10EconomicalBowlers(matches, deliveries),
  numOfTimesEachTeamOwnTossMatch(matches),
  topPlayerOfTheMatch(matches),
  strikeRateBatsman(matches, deliveries),
  highestNumTimesPlayerDismissed(deliveries),
  bestEconomyBowlerSuperOver(deliveries)
);
