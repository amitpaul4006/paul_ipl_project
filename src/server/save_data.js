const fs = require('fs');

//out put path
const numOfMatchesPlayedPerYearPath     = './src/public/output/num_of_matches_played_per_year.json';
const numOfMatchOwnPerTeamPerYearPath   = './src/public/output/num_of_matches_own_per_team_per_year.json';
const extraRunPerTeamPath               = './src/public/output/extra_run_per_team.json';
const top10EconomicalBowlersPath        = './src/public/output/top_10_economical_bowlers.json';
const numOfTimesEachTeamOwnTossMatchPath= './src/public/output/num_times_each_team_own_toss_match.json';
const topPlayerOfTheMatchPath           = './src/public/output/top_player_of_the_match.json';
const strikeRateEachBatsmanPath         = './src/public/output/strike_rate_batsman_each_season.json';
const highestNumTimesPlayerDismissedPath= './src/public/output/highest_num_times_player_dismissed.json';
const best_economy_bowler_superOverPath = './src/public/output/best_economy_bowler_superOver.json';

const saveData = (numOfMatchesPlayedPerYear, 
                  numOfMatchOwnPerTeamPerYear, 
                  extraRunPerTeam, 
                  top10EconomicalBowlers, 
                  numTimeEachTeamOwnTossMatch,
                  topPlayerOfTheMatch,
                  strikeRateEachBatsman,
                  highestNumTimesPlayerDismissed,
                  bestEconomyBowlerSuperOver
                  
)=>{
    
    // json string data.
    const num_of_matches_played_per_year_data       = JSON.stringify(numOfMatchesPlayedPerYear,null,4);
    const num_of_matches_own_per_team_per_year_data = JSON.stringify(numOfMatchOwnPerTeamPerYear,null,4);
    const extra_run_per_team_data                   = JSON.stringify(extraRunPerTeam,null,4);
    const top_10_economical_bowlers_data            = JSON.stringify(top10EconomicalBowlers,null,4);
    const num_times_each_team_own_toss_match_data   = JSON.stringify(numTimeEachTeamOwnTossMatch,null,4);
    const top_player_of_the_match_data              = JSON.stringify(topPlayerOfTheMatch,null,4);
    const strike_rate_each_batsman_data             = JSON.stringify(strikeRateEachBatsman,null,4);
    const highest_num_times_player_dismissed_data   = JSON.stringify(highestNumTimesPlayerDismissed);
    const best_economy_bowler_superOver_data        = JSON.stringify(bestEconomyBowlerSuperOver);

    
    
    //write function.
    fs.writeFileSync(numOfMatchesPlayedPerYearPath, num_of_matches_played_per_year_data, 'utf8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(numOfMatchOwnPerTeamPerYearPath, num_of_matches_own_per_team_per_year_data, 'utf8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(extraRunPerTeamPath, extra_run_per_team_data, 'utf8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(top10EconomicalBowlersPath, top_10_economical_bowlers_data, 'utf8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(numOfTimesEachTeamOwnTossMatchPath, num_times_each_team_own_toss_match_data, 'utf8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(topPlayerOfTheMatchPath, top_player_of_the_match_data, 'utf-8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(strikeRateEachBatsmanPath, strike_rate_each_batsman_data, 'utf-8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(highestNumTimesPlayerDismissedPath, highest_num_times_player_dismissed_data, 'utf-8', (error)=>{
        if(error) console.log(error)
    });

    fs.writeFileSync(best_economy_bowler_superOverPath, best_economy_bowler_superOver_data, 'utf-8', (error)=>{
        if(error) console.log(error)
    });

}

module.exports = saveData;