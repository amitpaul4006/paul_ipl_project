const numberOfMatchesPlayedPerYear = (matches) => {
  const totalYear = matches.reduce((matchYearObject, match) => {
    let year = match.season;
    if (matchYearObject[year]) matchYearObject[year] += 1;
    else matchYearObject[year] = 1;

    return matchYearObject;
  }, {});
  return totalYear;
};

module.exports = numberOfMatchesPlayedPerYear;
