const highestNumTimesplayer_dismissed = (objecDismissedPlayers) => {
  const dismissedPlayer = objecDismissedPlayers.reduce(
    (objecDismissedPlayer, del) => {
      const player_dismissed = del.player_dismissed;
      if (player_dismissed == "") {
        return objecDismissedPlayer;
      }
      const fielder = del.fielder;
      const bowler = del.bowler;
      const dissmissedBy = fielder || bowler;

      if (dissmissedBy != "" && player_dismissed != "") {
        if (objecDismissedPlayer[player_dismissed] !== undefined) {
          objecDismissedPlayer[player_dismissed] += 1;
        } else {
          objecDismissedPlayer[player_dismissed] = 1;
        }
      }
      return objecDismissedPlayer;
    },
    {}
  );

  let arrayDismissedPlayer = Object.entries(dismissedPlayer);
  let sortedDismissedPlayer = arrayDismissedPlayer
    .sort((a, b) => b[1] - a[1])
    .slice(0, 1);

  const highestDismissedPlayer = sortedDismissedPlayer.reduce(
    (objectDismissedPlayer, player) => {
      objectDismissedPlayer[player[0]] = player[1];
      return objectDismissedPlayer;
    },
    {}
  );

  return highestDismissedPlayer;
};

module.exports = highestNumTimesplayer_dismissed;
