function bowlersBestEconomyInSuperOvers(deliveries) {
  //calculating total runs given by a bowler.

  let totalRun = deliveries.reduce((runObject, del) => {
    let bowler = del.bowler;
    let runs = del.total_runs;
    if (bowler != "" && runs != 0) {
      if (runObject[bowler] !== undefined) {
        runObject[bowler] += parseInt(runs);
      } else {
        runObject[bowler] = parseInt(runs);
      }
    }
    return runObject;
  }, {});

  //calculating total ball bowled by bowler.

  let overBalled = deliveries.reduce((ballObject, del) => {
    let bowler = del.bowler;
    let superOver = del.is_super_over;
    if (bowler != "" && superOver != "") {
      if (ballObject[bowler] !== undefined) {
        if (superOver) ballObject[bowler] += 1;
      } else {
        ballObject[bowler] = 1;
      }
    }
    return ballObject;
  }, {});

  //Converting into array.

  let totalRunArr = Object.entries(totalRun);
  let overBalledArr = Object.entries(overBalled);

  // converting ball to over.

  overBalledArr.reduce((totalBall, over) => {
    over[1] = parseFloat((over[1] / 6).toFixed(2));
    return totalBall;
  }, []);

  //calculating economy of the bowler

  totalRunArr.reduce((totalRun, runs, index) => {
    runs[1] = parseFloat((runs[1] / overBalledArr[index][1]).toFixed(2));
    return totalRun;
  }, []);

  //sorting and fetch baller of best economy.
  totalRunArr.sort(function (a, b) {
    return a[1] - b[1];
  });
  totalRunArr = totalRunArr.slice(0, 1);

  //converting into object.

  const bestBowler = totalRunArr.reduce((ecoObject, elem) => {
    ecoObject[elem[0]] = elem[1];
    return ecoObject;
  }, {});
  return bestBowler;
}
module.exports = bowlersBestEconomyInSuperOvers;
