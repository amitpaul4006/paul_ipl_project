const strikeRateBatsman = (matches, deliveries) => {
  const totalRunAndBalls = deliveries.reduce((totalRunAndBallsObject, del) => {
    const fetchYearFromId = (id, matches) => {
      let arraySeason = matches.reduce((season, match) => {
        let year = match.season;
        if (id === match.id) season.push(year);
        return season;
      }, []);
      return arraySeason;
    };

    let ball = del.ball;
    let batsman = del.batsman;
    let id = del.match_id;
    let runs = del.batsman_runs;
    const year = fetchYearFromId(id, matches).toString();

    if (totalRunAndBallsObject.hasOwnProperty(batsman)) {
      if (totalRunAndBallsObject[batsman].hasOwnProperty(year)) {
        totalRunAndBallsObject[batsman][year].runs += parseInt(runs);
        totalRunAndBallsObject[batsman][year].ball += parseInt(ball);
      } else {
        totalRunAndBallsObject[batsman][year] = {};
        totalRunAndBallsObject[batsman][year].runs = parseInt(runs);
        totalRunAndBallsObject[batsman][year].ball = parseInt(ball);
      }
    } else {
      totalRunAndBallsObject[batsman] = {};
      totalRunAndBallsObject[batsman][year] = {};
      totalRunAndBallsObject[batsman][year].runs = parseInt(runs);
      totalRunAndBallsObject[batsman][year].ball = parseInt(ball);
    }
    return totalRunAndBallsObject;
  }, {});

  for (const key in totalRunAndBalls) {
    for (const key1 in totalRunAndBalls[key]) {
      totalRunAndBalls[key][key1] = (
        (totalRunAndBalls[key][key1].runs / totalRunAndBalls[key][key1].ball) *
        100
      ).toFixed(2);
    }
  }

  return totalRunAndBalls;

  // converting object to array
  // let arrayOfRunAndBalls  = Object.entries(totalRunAndBalls)

  // const strikeRate = arrayOfRunAndBalls.reduce((object, element)=>{

  //     let rateArray = []
  //     let seasonArray = []

  //     let seasons = Object.entries(element[1])
  //     seasonArray.push(seasons[0][0])

  //     let runAndBallarray = Object.entries((Object.entries(element[1]))[0][1]).reduce((run, ball)=> {
  //         let rate =  (((run[1] / ball[1]) * 100).toFixed(2))
  //         rateArray.push(rate)
  //     })
  //     object[seasonArray[0]] = rateArray[0]
  //     return object
  // },{})

  // console.log(strikeRate)
};

module.exports = strikeRateBatsman;
