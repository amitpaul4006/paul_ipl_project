const numOfMatchOwnTeamYear = (matches) => {
  const totalMatchesOwn = matches.reduce((totalMatchesOwnObject, match) => {
    let year = match.season;
    let winner = match.winner;
    if (totalMatchesOwnObject[year]) {
      if (totalMatchesOwnObject[year][winner]) {
        totalMatchesOwnObject[year][winner] += 1;
      } else {
        totalMatchesOwnObject[year][winner] = 1;
      }
    } else {
      totalMatchesOwnObject[year] = {};
    }
    return totalMatchesOwnObject;
  }, {});

  return totalMatchesOwn;
};
module.exports = numOfMatchOwnTeamYear;
