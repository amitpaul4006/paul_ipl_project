function top10EconomicalBowlers(matches, deliveries) {
  /**
   * Pushing match_id's for year 2015 into array.
   */
  let arr_match_id = [];
  const FetchId = matches.filter((obj) => {
    if (obj.season === "2015") {
      arr_match_id.push(obj.id);
    }
  });

  /**
   * Creating an object having bowler's name with there total runs.
   */
  let totalRun = deliveries.reduce((acc, deliverie) => {
    const bowler = deliverie.bowler;
    const match_id = deliverie.match_id;
    const total_runs = deliverie.total_runs;
    if (arr_match_id.includes(match_id)) {
      if (acc[bowler] !== undefined) {
        acc[bowler] += parseInt(total_runs);
      } else {
        acc[bowler] = parseInt(total_runs);
      }
    }
    return acc;
  }, {});

  /**
   * Creating an object having bolwer's name  with there ball count.
   */
  let overBalled = deliveries.reduce((acc, deliverie) => {
    const bowler = deliverie.bowler;
    const match_id = deliverie.match_id;
    if (arr_match_id.includes(match_id)) {
      if (acc[bowler] !== undefined) {
        acc[bowler] += 1;
      } else {
        acc[bowler] = 1;
      }
    }
    return acc;
  }, {});

  /** converting into array */
  let totalRunArr = Object.entries(totalRun);
  let overBalledArr = Object.entries(overBalled);

  /**
   * Logic to convert total balls into over
   */
  overBalledArr.reduce((acc, over) => {
    over[1] = parseFloat((over[1] / 6).toFixed(2));
    return acc;
  }, []);

  /**
   * Logic to find economy for bowlers.
   */
  totalRunArr.reduce((acc, runs, index) => {
    runs[1] = parseFloat((runs[1] / overBalledArr[index][1]).toFixed(2));
    return acc;
  }, []);

  /**
   * Sorting and slicing to find top economical bowler.
   */
  totalRunArr.sort(function (a, b) {
    return a[1] - b[1];
  });
  totalRunArr = totalRunArr.slice(0, 10);

  /**
   * Converting array into object and returning it.
   */
  const newResult = totalRunArr.reduce((acc, elem) => {
    acc[elem[0]] = elem[1];
    return acc;
  }, {});
  return newResult;
}

module.exports = top10EconomicalBowlers;
