const extraRunPerTeam = (matches, deliveries) => {
  let matchId = [];
  const FetchId = matches.filter((obj) => {
    if (obj.season === "2016") {
      matchId.push(obj.id);
    }
  });

  const totalExtraRun = deliveries.reduce((extraRunObject, del) => {
    let id = del.match_id;
    let extra_run = del.extra_runs;
    let team = del.bowling_team;
    if (matchId.includes(id)) {
      if (extraRunObject[team]) {
        extraRunObject[team] += parseInt(extra_run);
      } else {
        extraRunObject[team] = parseInt(extra_run);
      }
    }
    return extraRunObject;
  }, {});
  return totalExtraRun;
};

module.exports = extraRunPerTeam;

